<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Answer</title>
</head>
<body>

<p>SOLUTION = 
    <?php
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    if($_POST['group1'] == 'add') {
        echo $num1 + $num2;
    }
    else if($_POST['group1'] == 'subtract') {
        echo $num1 - $num2;
    }
    else if($_POST['group1'] == 'multiply') {
        echo $num1 * $num2;
    }
    else if($_POST['group1'] == 'divide') {
        if ($num2==0){
            echo "IMPOSSIBLE! (division by 0)";
        }
        else{
            echo $num1 / $num2;
        }
    }
    ?>
</p>
</body>
</html>
